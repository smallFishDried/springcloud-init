# SpringCloud-Alibaba 项目初始模板

> 作者：xiaoyu

## 模块
```text
com.xiaoyu     
├── nacos-configs               // nacos共享配置，修改完打包成zip导入nacos中即可
├── springcloud-gateway         // 网关模块 [8080]
├── springcloud-admin           // 后台管理 [9001]
├── springcloud-auth            // 认证中心 [9200]
├── springcloud-common          // 通用模块
│     ├── springcloud-common-core          // 核心模块
│     ├── springcloud-common-dao           // 数据库的mapper和实体类
│     ├── springcloud-common-gen           // 生成代码
│     ├── springcloud-common-redis         // 缓存服务
│     └── springcloud-common-web           // web，给业务用的，注意：网关不能引此依赖
├── springcloud-service         // 业务模块 [9101-9199]
├── pom.xml                      // 公共依赖
```