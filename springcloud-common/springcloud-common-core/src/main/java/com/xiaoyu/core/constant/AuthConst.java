package com.xiaoyu.core.constant;

/**
 * 定义登录权限常量
 * @author ：小鱼
 * @date ：Created in 2023/7/8
 **/
public abstract class AuthConst {
    /**
     * jwt令牌ID
     */
    public static final String JWT_ID = "jti";
    /**
     * 用户ID
     */
    public static final String USER_ID = "uid";
    /**
     * 用户名
     */
    public static final String USER_NAME = "username";
}
