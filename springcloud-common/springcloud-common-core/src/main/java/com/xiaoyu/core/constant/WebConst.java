package com.xiaoyu.core.constant;

/**
 * 定义web常量
 * @author ：小鱼
 * @date ：Created in 2023/6/29
 **/
public abstract class WebConst {
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

}
