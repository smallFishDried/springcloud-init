package com.xiaoyu.core.domain;

import com.xiaoyu.core.enums.StatusCode;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * json数据返回标准结果对象
 * @author ：小鱼
 * @date ：Created in 2022/10/22
 **/
@NoArgsConstructor
@Data
public class Result<T> implements Serializable {
    /**
     * 自定义响应的状态码
     */
    private Integer code;
    /**
     * 响应的消息提示
     */
    private String message;
    /**
     * 响应的数据类型
     */
    private T data;

    private Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> Result<T> ok(){
        return new Result<T>(StatusCode.SUCCESS.getCode(),
                StatusCode.SUCCESS.getMsg(),null);
    }

    /**
     * 泛型方法：请求成功
     */
    public static <T> Result<T> ok(String message,T data){
        return new Result<T>(StatusCode.SUCCESS.getCode(), message,data);
    }

    /**
     * 请求成功
     */
    public static <T> Result<T> ok(T data){
        return new Result<T>(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMsg(),data);
    }

    /**
     * 请求失败
     */
    public static <T> Result<T> failure(){
        return new Result<T>(StatusCode.FAILURE.getCode(),StatusCode.FAILURE.getMsg(),null);
    }

    public static <T> Result<T> failure(String message,T data){
        return new Result<T>(StatusCode.FAILURE.getCode(),message,data);
    }

    public static <T> Result<T> failure(T data){
        return new Result<T>(StatusCode.FAILURE.getCode(),StatusCode.FAILURE.getMsg(),data);
    }

    /**
     * 错误，错误状态信息由开发者自己选择
     */
    public static <T> Result<T> error(StatusCode status,T data){
        return new Result<T>(status.getCode(),status.getMsg(),data);
    }
}
