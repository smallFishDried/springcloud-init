package com.xiaoyu.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 状态码枚举
 * @author ：小鱼
 * @date ：Created in 2022/10/22
 **/
@Getter
@AllArgsConstructor
public enum StatusCode {
    //定义枚举(定义选项)
    SUCCESS(200,"操作成功"),
    FAILURE(400,"请求失败"),
    //登录失败
    AUTH_ERROR(300,"登录失败"),
    //无权限操作
    NO_AUTHORIZATION(401,"未登录!"),
    //服务内部错误
    SERVER_ERROR(500,"服务内部错误!"),
    //网关错误
    GATEWAY_ERROR(502,"网关异常,稍后再试");

    /**
     * 状态码
     */
    private int code;

    /**
     * 消息
     */
    private String msg;
}
