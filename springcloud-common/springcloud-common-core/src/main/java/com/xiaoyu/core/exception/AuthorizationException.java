package com.xiaoyu.core.exception;

/**
 * 登录校验异常,这里一般是结合JWT令牌使用，当令牌解析报错时抛出此异常
 * @author ：小鱼
 * @date ：Created in 2023/6/12
 **/
public class AuthorizationException extends BuzzException {

    public AuthorizationException() {
    }

    public AuthorizationException(String message) {
        super(message);
    }

    public AuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }
}
