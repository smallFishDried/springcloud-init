package com.xiaoyu.core.exception;

/**
 * 业务异常,他是所有自定义异常的父类
 * @author ：小鱼
 * @date ：Created in 2023/6/12
 **/
public class BuzzException extends RuntimeException {
    public BuzzException() {
    }

    public BuzzException(String message) {
        super(message);
    }

    public BuzzException(String message, Throwable cause) {
        super(message, cause);
    }
}
