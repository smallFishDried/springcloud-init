package com.xiaoyu.core.jwt;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 装配jwt工具
 * @author ：小鱼
 * @date ：Created in 2023/6/30
 **/
@Configuration
@ConditionalOnProperty(prefix = "jwt",value = "enable",havingValue = "true")
@EnableConfigurationProperties(JwtProperties.class)
public class JwtConfig {
    @Bean
    public JwtUtil jjwtUtil(JwtProperties jwtProperties){
        return JwtUtil
                // 创建构建者
                .builder()
                //加载配置
                .config(jwtProperties)
                //创建jwt工具
                .build();
    }
}
