package com.xiaoyu.core.jwt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DurationUnit;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * 读取jwt配置
 * @author ：小鱼
 * @date ：Created in 2023/3/27
 **/
@Data
@ConfigurationProperties(prefix = "jwt")
public class JwtProperties {
    /**
     * 是否装配jwt令牌工具bean,如果不写默认为不装配,为true则装配
     */
    private Boolean enable;
    /**
     * 生成jwt的秘钥,秘钥长度不能低于256bit
     */
    private String secret;

    /**
     * 过期时间(默认600秒),如果不带时间单位,默认单位为秒
     */
    @DurationUnit(ChronoUnit.SECONDS)
    private Duration expire = Duration.ofSeconds(600);

    /**
     * 过期时间记录在令牌中json中,配置过期时间值对应的key名称
     */
    private String expireKey = "token_expire";
    /**
     * 令牌主题
     */
    private String subject = "login-token";
    /**
     * 令牌在响应头中的配置
     */
    private Header header = new Header();

    @Data
    public static class Header{
        /**
         * 令牌传输是基于请求头,设置请求头-key
         */
        private String key;
        /**
         * 令牌前缀,默认为Bearer
         */
        private String tokenPrefix = "Bearer";
    }
}
