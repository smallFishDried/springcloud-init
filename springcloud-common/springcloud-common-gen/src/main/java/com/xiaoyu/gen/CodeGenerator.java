package com.xiaoyu.gen;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Author：xiaoyu
 * @data: 2023/8/10 21:56
 */
public class CodeGenerator {

    //数据库url
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/yuoj?characterEncoding=UTF-8&useUnicode=true&useSSL=false";

    //数据库用户名
    private static final String USERNAME = "root";

    //数据库密码
    private static final String PASSWORD = "123456";

    //作者
    private static final String AUTHOR = "xiaoyu";

    //日期格式
    private static final String DATE = "yyyy/MM/dd hh:mm";

    //根目录
    private static final String DIR = System.getProperty("user.dir") + "/springcloud-common/springcloud-common-dao/src/main";

    //类目录
    private static final String CLASS_DIR = DIR + "/java";

    //xml目录
    private static final String XML_DIR = DIR + "/resources/mappers";

    //父包名
    private static final String PARENT_PACKAGING = "com.xiaoyu.dao";

    //mapper包名,如果要使用指定报名的话就不能用list表名
    private static final String MAPPER_PACKAGING = "mapper.post";

    //entity包名,与mapper包名同理
    private static final String ENTITY_PACKAGING = "entity.post";

    //表名,与指定mapper和entity包名同用,如果不分那么仔细可以直接用 TABLE_NAMES
    private static final String TABLE_NAME = "^post.*";

    //表名,支持正则匹配、例如 ^post_.* 所有 post_ 开头的表名,如果要用这个必须把86-87注释
    private static final List<String> TABLE_NAMES = Arrays.asList("user", "^post.*");

    //过滤前缀,去掉 post_
    private static final String PREFIX = "";

    public static void main(String[] args) {
        FastAutoGenerator.create(URL, USERNAME, PASSWORD)
                // 全局配置
                .globalConfig(builder -> {
                    builder.author(AUTHOR) // 设置作者
                            .commentDate(DATE)   //注释日期
                            .outputDir(CLASS_DIR) // 指定输出目录
                            .disableOpenDir() //禁止打开输出目录，默认打开
                    ;
                })
                // 模板配置，如果需要生成xml、controller、service就全部注释
                /**
                 * 放开的注释行数：
                 *  xml生成配置：92
                 *  service：110-113
                 *  controller：115-116
                 * 添加的注释行数
                 *  禁用xml模板：83
                 */
                .templateConfig(builder -> {
                    builder.disable(TemplateType.CONTROLLER) //不生成controller
                            .disable(TemplateType.SERVICE) //不生成service
                            .disable(TemplateType.SERVICE_IMPL) //不生成serviceImpl
                            .disable(TemplateType.XML) //不生成xml
                    ;
                })
                // 包配置
                .packageConfig(builder -> {
                    builder.parent(PARENT_PACKAGING) // 设置父包名
                            //这两个如果用 TABLE_NAMES 的话必须注释掉
                            .mapper(MAPPER_PACKAGING)
                            .entity(ENTITY_PACKAGING)
                            //.pathInfo(Collections.singletonMap(OutputFile.xml, XML_DIR)) // 一般不推荐生成xml，去自己的模块写就好了 设置mapper.xml生成路径
                    ;
                })
                // 策略配置
                .strategyConfig(builder -> {
                    builder.addInclude(TABLE_NAME) // 设置需要生成的表名
                            .addTablePrefix(PREFIX) // 设置过滤表前缀
                            // Entity 策略配置
                            .entityBuilder()
                            .enableLombok() //开启 Lombok
                            .enableFileOverride() // 覆盖已生成文件
                            .naming(NamingStrategy.underline_to_camel)  //数据库表映射到实体的命名策略：下划线转驼峰命
                            .columnNaming(NamingStrategy.underline_to_camel)    //数据库表字段映射到实体的命名策略：下划线转驼峰命
                            // Mapper 策略配置
                            .mapperBuilder()
                            .enableFileOverride() // 覆盖已生成文件
                            //如果需要生成service和controller放开注释，一般不推荐跟xml一样去自己的模块写就好了
                            // Service 策略配置
                            /*.serviceBuilder()
                            .enableFileOverride() // 覆盖已生成文件
                            .formatServiceFileName("%sService") //格式化 service 接口文件名称，%s进行匹配表名，如 UserService
                            .formatServiceImplFileName("%sServiceImpl") //格式化 service 实现类文件名称，%s进行匹配表名，如 UserServiceImpl*/
                            // Controller 策略配置
                            /*.controllerBuilder()
                            .enableFileOverride() // 覆盖已生成文件*/
                    ;
                })
                .execute();
    }

}
