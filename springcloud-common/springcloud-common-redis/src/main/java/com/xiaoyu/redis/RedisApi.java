package com.xiaoyu.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.*;

import java.util.concurrent.TimeUnit;

/**
 * 自定义Redis操作面板
 * @author ：小鱼
 * @date ：Created in 2022/11/30
 **/
@Slf4j
public class RedisApi {

    private RedisTemplate redisTemplate;
    /**
     * 操作String类型数据结构
     */
    private ValueOperations opsValue;
    /**
     * 操作hash结构
     */
    private HashOperations opsHash;
    /**
     * 操作列表结构
     */
    ListOperations opsList;
    /**
     * 操作集合结构
     */
    SetOperations opsSet;
    /**
     * 操作有序集合结构
     */
    ZSetOperations opsZset;

    public RedisApi(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.opsValue = redisTemplate.opsForValue();
        this.opsHash = redisTemplate.opsForHash();
        this.opsList = redisTemplate.opsForList();
        this.opsSet = redisTemplate.opsForSet();
        this.opsZset = redisTemplate.opsForZSet();
    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    /*-------------------------------操作String结构api----------------------------------*/
    /**
     * 返回指定key的值
     */
    public <T> T get(String key){
        return (T) opsValue.get(key);
    }

    /**
     * 设置key的过期时间
     * @param key
     * @param timeout
     * @param unit
     * @return
     */
    public Boolean ttl(String key,final long timeout, final TimeUnit unit){
        return redisTemplate.expire(key,timeout,unit);
    }

    /**
     * 设置键值对
     */
    public void set(String key,Object value){
        opsValue.set(key,value);
    }

    /**
     * 设置带过期时间的键值对
     */
    public void set(String key, Object value, Long time, TimeUnit timeUnit){
        opsValue.set(key,value,time,timeUnit);
    }

    /**
     * 当key不存在时，设置key-value，并且设置过期时间
     * @param key
     * @param value
     * @param time
     * @param timeUnit
     * @return
     */
    public Boolean setNx(String key, Object value,long time,TimeUnit timeUnit){
        return this.opsValue.setIfAbsent(key,value,time,timeUnit);
    }

    /**
     * 删除指定key
     */
    public Boolean del(String key){
        return redisTemplate.delete(key);
    }

    /**
     * 生成自增序列
     */
    public Integer incrby(String key,Integer increment){
        return opsValue.increment(key, increment).intValue();
    }

    /*-------------------------------操作List结构api----------------------------------*/

    /**
     * 从表头压入元素
     */
    public Long lpush(String key,Object... value){
        return opsList.leftPushAll(key, value);
    }

}
