package com.xiaoyu.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * redis配置类
 * @author ：小鱼
 * @date ：Created in 2022/11/29
 **/
@Configuration
@AutoConfigureBefore(RedisAutoConfiguration.class)
public class RedisConfig {

    /**
     * 装配redis面板
     */
    @Bean
    public RedisTemplate redisTemplate(RedisConnectionFactory redisConnectionFactory){
        //todo 1.创建redis操作面板&绑定连接工厂
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        //todo 2.设置key的序列化器,key一般用字符串
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        //todo 3.设置hash-field域的序列化器,field一般用字符串
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        //todo 4.设置value的序列化器
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer());
        //todo 5.设置hash-value序列化器
        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer());
        return redisTemplate;
    }

    /**
     * 装配自定义的jackson序列化器
     */
    @Bean
    public RedisSerializer jackson2JsonRedisSerializer(){
        //todo 1.自定义值序列化器
        Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<Object>(Object.class);
        //todo 2.jackson工具类
        ObjectMapper objectMapper = new ObjectMapper();
        //递推子类的属性也序列化成json
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        //objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        //声明-对象类型与对象数据都将序列化到redis当中
        objectMapper.activateDefaultTyping(
                LaissezFaireSubTypeValidator.instance
                ,ObjectMapper.DefaultTyping.NON_FINAL);
        //忽略对象中为null的字段,不进行序列化
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        //todo 3.绑定序列化器与jackson工具
        serializer.setObjectMapper(objectMapper);
        return serializer;
    }

    @Bean
    public RedisApi redisApi(@Qualifier("redisTemplate") RedisTemplate redisTemplate){
        return new RedisApi(redisTemplate);
    }
}
