package com.xiaoyu.web.sentinel;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//spring.cloud.sentinel.enabled这个条件为true才装配
@ConditionalOnProperty(name = "spring.cloud.sentinel.enabled", matchIfMissing = true)
//必须装配了SentinelAutoConfiguration才装配
@ConditionalOnClass(name = {
        "com.alibaba.cloud.sentinel.custom.SentinelAutoConfiguration"
})
//@AutoConfigureAfter(SentinelAutoConfiguration.class)
public class SentinelDataSourceConfig {

    /*@Bean
    @ConditionalOnMissingBean
    public SentinelDataSourceHandler sentinelDataSourceHandler(
            DefaultListableBeanFactory beanFactory,
            SentinelProperties sentinelProperties,
            Environment env) {
        return new SentinelDataSourceHandler(beanFactory, sentinelProperties, env);
    }*/

    /**
     * 必须保证SentinelDataSourceHandler先于RegistryNacosDatasourceHandler装配
     * 否则RegistryNacosDatasourceHandler中拿不到NacosDataSource这个bean对象
     */
    @Bean
    //@DependsOn("sentinelDataSourceHandler")
    public RegistryNacosDatasourceHandler registryNacosDatasourceHandler(DefaultListableBeanFactory beanFactory) {
        return new RegistryNacosDatasourceHandler(beanFactory);
    }

}
